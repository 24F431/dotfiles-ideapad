#!/bin/bash
# script to comment out non printable characters
# new filename
f=emoji-check.txt
# clear file
> $f
IFS=$'\n'
while read j; do
# check for non printable characters
  x=$(echo "$j" | grep '[^[:print:]]')
  if [[ -z "$x" ]]; then
    echo $j >> $f
  else
    # write with # [comment]
    echo '# '$j >> $f
  fi
done < emoji.txt
