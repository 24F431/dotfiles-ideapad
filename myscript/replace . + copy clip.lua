local params = {...}
orig = params[1]
--~ local orig = "text.dfgfgj.loui� [1080p]69[ghj].mkv"
--~ DC.LogWrite(orig)
--~ Endung -> ""
--~ new, n = string.gsub(orig, "%.[^.]*$", "", 1)
--~ . -> " "
new, n = string.gsub(orig, "[.]", " ")
--~ _ -> " "
new, n = string.gsub(new, "[_]", " ")
--~ DC.LogWrite(new)
--~ zwischen [] -> ""
new, n = string.gsub(new, "%[[^%]]*%]", "")
--~ DC.LogWrite(new .. " wird kopiert", 0, true, false)
--~ Dialogs.MessageBox(new .. " wird kopiert", "Info", 0x0000)
--~ copy to clip
--~ io.popen('clip', 'w'):write(new):close()
Clipbrd.SetAsText(new)
