# turn greeting off
set fish_greeting
# custom prompt
function fish_prompt
    set -l textcol green
    set_color $textcol
    printf $USER
    set -l textcol yellow
    set_color $textcol
    printf @
    set -l textcol blue
    set_color $textcol
    printf (hostname)
    set -l textcol yellow
    set_color $textcol
    printf 🐠:
    set -l textcol white
    set_color $textcol
    echo (prompt_pwd)
    printf '$ '
end
# add fish source in kitty
if echo $TERM | grep -iq 'kitty'
    kitty + complete setup fish | source
end
# Alias definitions.
alias mydate='date +%F_%H-%M-%S'
alias desk='cd /home/raf/Schreibtisch/'
alias op='opera --private'
alias cls='clear'
alias myip='ip -4 -c addr'
alias xclip="xclip -selection c"
alias catc='highlight --out-format=ansi'
alias hl='highlight --out-format=ansi'
alias hlsh='highlight --out-format=ansi --syntax=sh'
alias dff='df -h --exclude-type=squashfs'
alias c='clear'
alias jd='echo JDownloader starten; /bin/sh /home/raf/jd2/JDownloader2 &'
alias pi2='ssh pi@192.168.178.46'
alias r='ranger'
alias kal='ncal -3wb'
alias ts='printf "%sx%s\n" (tput cols) (tput lines)'
alias smeth=' speedometer -r enp2s0'
alias mylsblk='lsblk --exclude 7'
alias vali='vim $HOME/.bash_aliases'
alias mygit='/usr/bin/git --git-dir=$HOME/dotfiles-ideapad --work-tree=$HOME'
alias emoji-clip='eval $HOME/myscript/emoji-clip.sh'
alias mydatex="date +%F_%H-%M-%S | tr -d '\n' | tee (tty) | xclip"
