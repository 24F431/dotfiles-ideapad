# Alias definitions.
alias mydate='date +%F_%H-%M-%S'
alias desk='cd /home/raf/Schreibtisch/'
alias op='opera --private'
alias cls='clear'
alias myip='ip -4 -c addr'
alias xclip="xclip -selection c"
alias mydatex="date +%F_%H-%M-%S| tr -d $'\n'| tee >(xclip) && printf '\n'"
alias mytilix='tilix -s /home/raf/Dokumente/tilix_profiles/beste.json'
alias catc='highlight --out-format=ansi'
alias hl='highlight --out-format=ansi'
alias hlsh='highlight --out-format=ansi --syntax=sh'
alias dff='df -h --exclude-type=squashfs'
alias c='clear'
alias s='source $HOME/.bashrc'
alias jd='echo JDownloader starten; /bin/sh /home/raf/jd2/JDownloader2 &'
alias pi2='ssh pi@192.168.178.46'
alias r='ranger'
alias pp='ifs="$IFS"; IFS=:; for p in $PATH; do echo $p; done; IFS="$ifs"'
alias akt='sudo -- bash -c "figlet system update | /usr/games/lolcat &&
  apt update && apt upgrade -y && apt clean && figlet OK | /usr/games/lolcat ||
  figlet fail | /usr/games/lolcat"'
alias kal='ncal -3wb'
alias ts='printf "%sx%s\n" $(tput cols) $(tput lines)'
alias smeth=' speedometer -r enp2s0'
alias mylsblk='lsblk --exclude 7'
alias vali='vim $HOME/.bash_aliases'
alias mygit='/usr/bin/git --git-dir=$HOME/dotfiles-ideapad --work-tree=$HOME'
alias emoji-clip='$HOME/myscript/emoji-clip.sh'
